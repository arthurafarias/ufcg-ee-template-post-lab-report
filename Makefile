#TMPDIR=./tmp
OUTDIR=build
MAINTEX:=main.tex

.PHONY: $(MAINTEX) all

all: $(MAINTEX)

$(MAINTEX):
	latexmk -pdf -xelatex -jobname=$(pathsubst %.tex,,$@) -outdir=$(OUTDIR) $@

clean:
	@echo "Cleaning..."
	@find $(OUTDIR) | grep -E "\.(aux|bbl|log|toc|xdv|blg|pdf|bcf|out|ptc|run.xml|idx|fls|fdb_latexmk|ilg|ind)" | xargs rm -f -v
	@echo "Done!"

commit:
	git add .
	git commit

push:
	git remote | xargs -L1 git push --all
